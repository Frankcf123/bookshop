<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class generalsql_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getLatestThree() {
        $this->db->select('*');
        $this->db->from('book');
        $this->db->limit(3, 0);
        $this->db->order_by('book_id', 'DESC');
        return $this->db->get()->result();
    }

    public function getAllCategories() {
        $this->db->select('*');
        $this->db->from('category');

        return $this->db->get()->result();
    }

    public function getBooksByCategory() {
        //$id = $this->input->get("categoryid");
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('category_id', 1);
        return $this->db->get()->result();
    }

    public function getBooksByCategoryPage() {
        // $id = $this->input->get("categoryid");
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('category_id', 1);
        $this->db->limit(7, $this->uri->segment(3));
        return $this->db->get()->result();
    }

    public function getGiftCatd() {
        $this->db->select('*');
        $this->db->from('giftcard');
        return $this->db->get()->result();
    }

   
}
