
<div class="container">

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Check out

            </h1>
            <ol class="breadcrumb">
                <li>   <a href=<?php echo base_url(); ?> > Home</a>
                </li>
                <li>Check out</li>

            </ol>
        </div>
    </div>
    <div class="container">
        <?php echo form_open(base_url() . 'index.php/checkout_controller/newview'); ?>

        <fieldset>
            <legend>Delivery Address  
                <p class="text-warning">Sorry ,Currently,we only support cash</p></legend>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="name">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" id="card-holder-name" 

                           placeholder="name">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="address">Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address" id="card-holder-name" 
                           placeholder="address">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="state">State</label>
                <div class="col-sm-9">
                    <select class="form-control" name="state">
                        <option value="ACT">ACT</option>
                        <option value="NSW">NSW</option>
                        <option value="NT">NT</option>
                        <option value="QLD">QLD</option>
                        <option value="SA">SA</option>
                        <option value="TAS">TAS</option>
                        <option value="VIC">VIC</option>
                        <option value="WA">WA</option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="postcode">Post code</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" id="card-holder-name" 

                           placeholder="postcode">
                </div>
            </div>
               <legend> 
                <p class="text-warning" align="right"><?php echo $error; ?></p></legend>
            
            <div align="right">
                <button class="btn btn-success" type="submit">Confirm</button>
            </div>
        </fieldset>
        </form>
    </div>
    <br/><br/>
    <br/><br/>
    <br/><br/>
    <br/><br/>
