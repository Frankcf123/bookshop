
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">About us

            </h1>
            <ol class="breadcrumb">
                <li>   <a href=<?php echo base_url(); ?> > Home</a>
                </li>
                <li class="active">About us</li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <br/><br/>

    <!-- Intro Content -->
    <div class="row">
        <div class="col-md-6">
            <img class="img-responsive" src="/wda/images/systemImage/logo.jpg" alt="">
        </div>
        <div class="col-md-6">
            <h2>About ASDF-Online Bookstore</h2>
            <p>We are the store to sell the book all over the world. Currently we do not support the credit card, 
            only cash is allowed. We got more than 1000 books in our store. </p>
        </div>
    </div>
    <!-- /.row -->

    <!-- Team Members -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Our Team</h2>
        </div>
        <div class="col-md-4 text-center">
            <div class="thumbnail">
                <img class="img-responsive" src="/wda/images/systemImage/person5.jpg"  alt=""
                       width="100" height="100">
                <div class="caption">
                    <h3>Bruce<br>
                        <small>Job Title:Team Leader, Front End Developer </small>
                    </h3>
                   
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="thumbnail">
                <img class="img-responsive" src="/wda/images/systemImage/person4.jpg"  alt=""
                       width="100" height="100">
                <div class="caption">
                    <h3>Charles<br>
                        <small>Job Title:Back-end Programmer,Designer</small>
                    </h3>
                  
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="thumbnail">
                <img class="img-responsive" src="/wda/images/systemImage/person3.jpg" alt=""
                      width="100" height="100">
                <div class="caption">
                    <h3>Frank<br>
                        <small>Job Title:Back-end Programmer</small>
                    </h3>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="thumbnail">
                <img class="img-responsive" src="/wda/images/systemImage/person2.jpg"  alt=""
                        width="100" height="100">
                <div class="caption">
                    <h3>Jas<br>
                        <small>Job Title:Database designer</small>
                    </h3>
                   
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="thumbnail">
                <img class="img-responsive" src="/wda/images/systemImage/person1.jpg"  alt=""
                       width="100" height="100">
                <div class="caption">
                    <h3>Zhang<br>
                        <small>Job Title: Front End Developer </small>
                    </h3>
                   
                </div>
            </div>
        </div>
      
    </div>
    <!-- /.row -->

    <!-- Our Customers -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Our Customers</h2>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <img class="img-responsive customer-img" src="/wda/images/systemImage/company1.jpg" alt="">
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <img class="img-responsive customer-img" src="/wda/images/systemImage/company2.jpg" alt="">
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <img class="img-responsive customer-img" src="/wda/images/systemImage/company3.jpg" alt="">
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <img class="img-responsive customer-img" src="/wda/images/systemImage/company4.jpg" alt="">
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <img class="img-responsive customer-img" src="/wda/images/systemImage/company5.jpg" alt="">
        </div>
      
    </div>
    <br/><br/>    <br/><br/>