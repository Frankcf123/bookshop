
<div class="container">

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Shopping Cart

            </h1>
            <ol class="breadcrumb">
                <li>   <a href=<?php echo base_url(); ?> > Home</a>
                </li>
                <li>Shopping Cart</li>

            </ol>
        </div>
    </div>
    <!-- /.row -->
    <?php echo form_open(base_url() . 'index.php/shoppingcart_controller/update') ?>

    <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table table-striped">

        <tr class="success">

            <th>Item Name</th>

            <th>Unit price</th>
            <th>Item Quantity($)</th>
            <th>Subtotal($)</th>
            <th></th>
            <th></th>

        </tr>
        <?php
        $this->load->library('cart');
        foreach ($this->cart->contents() as $items) {
            echo "<tr>";
            ?>
            <td><?php echo $items['name']; ?></td>
            <input type="hidden" value="<?php echo $items['rowid']; ?>" name="<?php echo $items['rowid'] ?>"></td>
            <td><?php echo $items['price']; ?></td>
            <td><input type="number" min=0 value="<?php echo $items['qty']; ?>" name="<?php echo $items['rowid'] . '_qty' ?>"></td>
            <td><?php
                echo $items['subtotal'];
                ?></td>
            <td><button class="btn btn-success" type="submit">Update</button></td>
            <td>        
                <a href="<?php echo base_url() . 'index.php/shoppingcart_controller/delete/' . $items['rowid'] ?>">
                   Delete</a></td>
          
            <?php
            echo "</tr>";
        }
        ?>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><h3>Total:</h3></td>
            <td> <h3>$<?php $this->load->library('cart');
        echo $this->cart->total();
        ?> </h3> </td></tr>
    </table>   
</form>
<div align="right"> 
<a href="<?php echo base_url() . 'index.php/shoppingcart_controller/clear' ?>">
        <button class="btn btn-primary">
            Clear Shopping Cart
        </button></a>
    <a href="<?php echo base_url() . 'index.php/checkout_controller/index' ?>">
        <button  class="btn btn-primary">
            Check Out
        </button></a>
</div>

<br/><br/><br/><br/>
