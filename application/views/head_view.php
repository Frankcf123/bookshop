<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to ASDF Online Bookstore</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

       

        <link href="/wda/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/wda/bootstrap/css/style.css" rel="stylesheet">

       
         <!--Fav and touch icons--> 
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/wda/bootstrap/img/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/wda/bootstrap/img/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/wda/bootstrap/img/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="wda/bootstrap/img/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="/wda/bootstrap/img/favicon.png">

        <script type="text/javascript" src="/wda/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="/wda/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/wda/bootstrap/js/scripts.js"></script>
<script type='text/javascript'>
function validateForm() {
    var x = document.forms["myForm"]["fname"].value;
    if (x == null || x == "") {
        alert("First name must be filled out");
        return false;
    }
}
       
$(document).ready(function() {
    $('#profileForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fullName: {
                validators: {
                    stringLength: {
                        max: 50,
                        message: 'The full name must be less than 50 characters'
                    }
                }
            },
            bio: {
                validators: {
                    stringLength: {
                        max: 200,
                        message: 'The bio must be less than 200 characters'
                    }
                }
            }
        }
    });
});
 </script>

