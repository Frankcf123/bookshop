
<div class="container">

    <!-- /.row -->
   <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Home
                 
                </h1>
                <ol class="breadcrumb">
                    <li>   <a href=<?php echo base_url() ;?> > Home</a>
                    </li>
                    <li><?php echo $title; ?></li>
                   
                </ol>
            </div>
        </div>
        <!-- /.row -->
    <!-- Project One -->
    <?php foreach ($query->result() as $book) {
        ?>
        <div class="row">
            <div class="col-md-7">
                <a href="">
                    <img class="img-responsive img-hover" 
                     src="<?php echo '/wda/images/bookcover/'.$book->book_id.".jpg";?>" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h1><?php echo $book->book_name; ?></h1>
                <h5>category: 
                <?php 
                $this->db->select('category');
                $this->db->from('category');
                $this->db->where('category_id',$book->category_id);
                
                $result=$this->db->get()->result();
                $re=$result[0];
                echo $re->category;
                ?>
                </h5>
                <h5>author: <?php echo $book->book_author; ?></h5>
                <h5>price: $<?php echo $book->price; ?></h5>
                
            <a class="btn btn-primary btn-large" href=<?php echo base_url()."index.php/book_controller/index/".$book->book_id ; ?>>View Details</i></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php
    }

    echo "<br/><br/>".$this->pagination->create_links();
    ?>
<br/><br/><br/><br/>
