
<body>
    <?php
    if (!isset($_SESSION)) {
        session_start();
    }
    ?>
    <div class="container">

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php
                  if (isset($_SESSION['username'])) {
                ?>
                
                <li class="active">
                    <a href=<?php echo base_url() . "index.php/shoppingcart_controller/index"; ?> >Shopping Cart(<?php
              
                    echo $this->cart->total_items();

                ?>)</a>
                </li>
                  <li><?php }
                    if (isset($_SESSION['username'])) {
                        ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                            <?php
                            echo $_SESSION['username'];
                            ?><strong class="caret"></strong></a>
                        <ul class="dropdown-menu">
                            <li class="active"><a href=<?php echo base_url() . "index.php/LoginController/Logout" ?>>

                                    Logout</a></li>                                              
                        <?php } else { ?>
                            <a id="modal-924317" href="#modal-container-924317" role="button" class="btn" data-toggle="modal">Login</a>
<?php } ?>    
                        <div class="modal fade" id="modal-container-924317" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">
                                            Login for ASDF Online Bookstore
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <!--<form class="form-horizontal" role="form">-->
<?php echo form_open(base_url() . 'index.php/LoginController/verifyUser') ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">UserName</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="username"
                                                       name="username">
                                            </div>
                                        </div>
                                        <br/><br/><br/>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password"
                                                       name="password">
                                            </div>
                                        </div>
                                        <br/><br/><br/>

                                  
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-default">Sign in</button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <a href=<?php echo base_url() . "index.php/reg_controller"; ?> >Do not have an account, go to register </button></a>
                                    </div>
                                </div>	
                            </div>
                        </div>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>

<br/><div style="text-font:99;text-size:900px;text-align:center;background: "><h1> ASDF Book shop</h1></div>
<br/>
<br/>

<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <div class="row clearfix">
                        <div class="col-md-6 column">
                        </div>
                        <div class="col-md-3 column">
                        </div>
                        <div class="col-md-3 column">
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href=<?php echo base_url(); ?> >Home</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Category<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
<?php foreach ($categories as $cat) { ?>
                                    <li>
                                        <a href=<?php echo base_url() . "index.php/result_category_controller/index/" . $cat->category_id . "/Category:" . $cat->category; ?> ><?php echo $cat->category; ?></a>
                                    </li>

<?php } ?>
                            </ul>
                      
                        <li>
                            <a href=<?php echo base_url() . "index.php/about_controller/index" ?>>About Us</a>
                        </li>
                        <li>
                            <a href=<?php echo base_url() . "index.php/contact_controller/index" ?>>Contact</a>
                        </li>
                        </li>
                    </ul>
                    <form class="navbar-form navbar-right" role="search"
                          action="<?php echo base_url().'index.php/search_controller/index'?>" 
                          method="post">
                        <div class="form-group">
                            <input type="text" class="form-control"
                            placeholder="Enter book Name" name="searchtext">
                        </div> <button type="submit" class="btn btn-default">search</button>
                    </form>
                </div>

            </nav>
        </div>
    </div>
