
<div class="container">

       <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $bookdetails[0]->book_name; ?>
                 
                </h1>
                <ol class="breadcrumb">
                    <li>   <a href=<?php echo base_url() ;?> > Home</a>
                    </li>
                    <li class="active"><?php echo $bookdetails[0]->book_name; ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    <br/><br/>


    <!-- Portfolio Item Row -->
    <div class="row">

        <div class="col-md-8">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="img-responsive" src="<?php echo '/wda/images/bookcover/'.$bookdetails[0]->book_id.".jpg";?>" alt="">
                    </div>
                    
                </div>

               
            </div>
        </div>

        <div class="col-md-4">
            <h3>Book Description</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
            <h3>author: <?php echo $bookdetails[0]->book_author; ?></h3>
            <h3>price:  $<?php echo $bookdetails[0]->price; ?></h3>
            <h3>Category: 
                <?php
                $category_id = $bookdetails[0]->category_id;
                $this->db->select('*');
                $this->db->from('category');
                $this->db->where('category_id', $category_id);
                $cat = $this->db->get()->result();
                $catName = $cat[0]->category;
                echo $catName;
                ?></h3>
        </div>
        <div class="row clearfix">
            <div class="col-md-4 column">
                <a class="btn btn-success" href=<?php
                echo base_url()."index.php/shoppingcart_controller/addBook/".$bookdetails[0]->book_id."/".str_replace(' ', '_', $bookdetails[0]->book_name)."/".$bookdetails[0]->price;
                
                ?> >Add to cart</a>
            </div>
        </div>			

    </div>
    <!-- /.row -->

    <!-- Related Projects Row -->
    <div class="row">

        <div class="col-lg-12">
            <h3 class="page-header">Related Book</h3>
        </div>

        <?php
        $category_id = $bookdetails[0]->category_id;
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('category_id', $category_id);
        $this->db->limit(4, 0);
        $related = $this->db->get()->result();


        foreach ($related as $rel) {
            ?>
            <div class="col-sm-3 col-xs-6">
                <a href=<?php echo base_url() . "index.php/book_controller/index/" . $rel->book_id; ?>>
                    <img class="img-responsive img-hover img-related"
height="50" width="100"
                         src="<?php echo '/wda/images/bookcover/'.$rel->book_id.".jpg";?>" alt=<?php echo $rel->book_name; ?> />
                         <?php echo $rel->book_name; ?>
                </a>
            </div>
        <?php } ?>


    </div>
    <div class="col-md-12 column">
        <h1 class="page-header">
        </h1>
    </div>
    <div class="well">
        <h4>Leave a Comment:</h4>
        <?php echo form_open(base_url().'index.php/book_controller/makeComments') ?>
            <div class="form-group">
                <textarea class="form-control" rows="3" name="comments"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <?php echo "<br/><br/>" . $this->pagination->create_links();
    ?>
    <?php
    foreach ($reviews->result() as $review) {
        ?>
        <div class="media">
            <a class="pull-left" href="#">
                <img class="media-object" src="http://placehold.it/64x64" alt="">
            </a>
            <div class="media-body">
                <h4 class="media-heading">
                   <?php echo $review->user_name; ?>
                    <!-- here for title/username  -->

                     <!-- here for date? -->

                </h4>
                <?php echo $review->content; ?>
            </div>
        </div>
    <?php } ?>

</div>
<div class="row clearfix">
    <div class="col-md-12 column">
        <h2 class="page-header">

        </h2>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12 column">
    </div>
</div>
</div>
</div>
