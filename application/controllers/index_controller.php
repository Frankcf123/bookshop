<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author chenfang
 */
class index_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->initPage();
    }

    public function initPage() {

        $this->load->view("head_view");

     $this->loadNavigation();
//        $this->load->view("index_view");
        $this->loadData();
        $this->load->view("footer_view");
    }
   
    public function loadData(){
              $this->load->model("generalsql_model");

        $data['lastThree']=$this->generalsql_model->getLatestThree();
        $this->load->view("index_view",$data);

    }
    
    public function loadNavigation(){
        $this->load->model("generalsql_model");
        $data['categories']=$this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view",$data);
    }
   
    
    //put your code here
}
