<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of shoppingcart_controller
 *
 * @author user
 */
class shoppingcart_controller extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->initPage();
    }

    public function initPage() {
        $this->load->view("head_view");
        $this->load->model("generalsql_model");
        $this->loadNavigation();
        $this->load->view('shoppingCart_view');
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function addBook($bookid, $bookname, $price) {

        $this->load->library('cart');
//     $bookname="quis_lectus_suspendisse";
        $key = "Some key";
        $encypted = $this->encrypt->encode($bookname, $key);


        $data = array(
            'id' => "item" . $bookid,
            'qty' => 1,
            'price' => $price,
            'name' => $bookname,
        );

        $this->cart->insert($data);
        $this->index();
    }

    public function update() {

        $this->load->library('cart');

        foreach ($this->cart->contents() as $items) {
            $rowid = $this->input->post($items['rowid']);
            $qty=$this->input->post($items['rowid'].'_qty');
            $data = array(
                'rowid' => $items['rowid'],
                'qty' => $qty
            );
            $this->cart->update($data);
        }
        $this->index();
    }

    public function delete($id=""){
        $this->load->library('cart');
        $data=array(
            "rowid"=>$id,
            "qty"=>0
        );
        $this->cart->update($data);
        $this->index();
    }
    
    public function clear(){
        $this->load->library('cart');

        foreach ($this->cart->contents() as $items) {
            $data = array(
                'rowid' => $items['rowid'],
                'qty' => 0
            );
            $this->cart->update($data);
        }
        $this->index();
    }
    

}
