<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category_controller
 *
 * @author chenfang
 */
class about_controller extends CI_Controller{
    //put your code here
    	
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->initPage();
    }

   
    public function initPage() {
        $this->load->view("head_view");
        $this->load->model("generalsql_model");
        $this->loadNavigation();
        $this->load->view("about_view");
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }
    //put your code here
}

