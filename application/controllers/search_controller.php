<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category_controller
 *
 * @author chenfang
 */
class search_controller extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $search = $this->input->post('searchtext');
        $this->initPage($search);
    }

    public function initPage($search = "") {
        $this->load->view("head_view");
        $this->load->model("generalsql_model");
        $this->loadNavigation();
        $this->loadSeach($search);
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function loadSeach($search = "") {

   
        
        
        $this->load->library('pagination');
        $config['base_url'] = base_url()."/index.php/search_controller/index/";
        $config['per_page'] = 4;
        $config['num_links'] = 5;
             $this->db->select('*');
        $this->db->from('book');
        $this->db->like('book_name',$search);
        $this->db->or_like('book_author',$search);
        $this->db->or_like('price',$search);
        $config['total_rows'] = $this->db->get()->num_rows();
         $this->db->select('*');
        $this->db->from('book');
        $this->db->like('book_name',$search);
        $this->db->or_like('book_author',$search);
        $this->db->or_like('price',$search);
        $this->db->limit(10,$this->uri->segment(4));
        $data['query']= $this->db->get();
       $this->pagination->initialize($config);
       $data['title']="RESULT PAGES";
       
        $this->load->view("result_view", $data);
    }

    //put your code here
}
