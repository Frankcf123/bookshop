<?php

class checkout_controller extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index($error = "") {
        $this->initPage($error);
    }

    public function initPage($error = "") {
        $this->load->view("head_view");
        $this->loadNavigation();
        $data['error'] = $error;
        $this->load->view("checkout_view", $data);
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function newview() {
        $name = $this->input->post('name');
        $address = $this->input->post('address');
        $state = $this->input->post('state');
        $postcode = $this->input->post('postcode');
        $error = "";
        if (empty($name) || empty($address) || empty($postcode)) {
            $error = "Please complet all fields";
            $this->initPage($error);
        } else if (preg_match("/^[^0-9]{4}$/", $postcode)) {
            $error = "post code can be only 4 digits";
            $this->initPage($error);
        } else {

            //update database
            $ins = array(
                'order_price' => $this->cart->total(),
                'order_address' => $address,
                'order_postcode' => $postcode,
                'order_state' => $state
            );
            $this->db->insert('order', $ins);

            //clear shopping cart
            $this->load->library('cart');
            foreach ($this->cart->contents() as $items) {
                $data = array(
                    'rowid' => $items['rowid'],
                    'qty' => 0
                );
                $this->cart->update($data);
            }
            $this->load->view("head_view");

            $this->loadNavigation();
//        $this->load->view("index_view");
            $this->loadData();
            $this->load->view("footer_view");
        }


      
    }

      public function loadData() {
            $this->load->model("generalsql_model");

            $data['lastThree'] = $this->generalsql_model->getLatestThree();
            $this->load->view("index_view", $data);
        }

}
