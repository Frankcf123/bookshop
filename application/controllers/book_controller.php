<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class book_controller extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index($id = "") {

        if (!isset($_SESSION)) {
            session_start();
        }
        $this->initPage($id);
        $_SESSION['book_id'] = $id;
    }

    public function initPage($id = "") {
        $this->load->view("head_view");
        $this->load->model("generalsql_model");
        $this->loadNavigation();
        $this->loadContent($id);
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function loadContent($id = "") {
        //load book details
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('book_id', $id);
        
        $data['bookdetails'] = $this->db->get()->result();


        //load the reviews 
        $this->load->library('pagination');
        $config['base_url'] = base_url() . "/index.php/book_controller/index/" . $id;
        $config['per_page'] = 3;
        $config['num_links'] = 5;
        $this->db->select('*');
        $this->db->from('review');
        $this->db->where('book_id', $id);
        $this->db->order_by('review_id','DESC');
        $config['total_rows'] = $this->db->get()->num_rows();
        $this->db->select('*');
        $this->db->from('review');
        $this->db->where('book_id', $id);
                $this->db->order_by('review_id','DESC');

        $this->db->limit(5, $this->uri->segment(4));
        $data['reviews'] = $this->db->get();
        $this->pagination->initialize($config);

        $this->load->view("book_view", $data);
    }

    public function makeComments() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $comment = $this->input->post('comments');
        $ins = array(
            'book_id' => $_SESSION['book_id'],
            'cust_id' => 2,
            'content' => $comment,
            'user_name'=>$_SESSION['username']
                
        );
        $this->db->insert('review', $ins);
        $this->index($_SESSION['book_id']);
    }

}
