<?php

class reg_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($error = "") {

        $this->initPage($error);
    }

    public function initPage($error = "") {

        $this->load->view("head_view");
        $this->loadNavigation();
        $data['error'] = $error;
        $this->load->view('register_view', $data);
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");
        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function verifyRegister() {
        $name = $this->input->post('usernamer');
        $password = $this->input->post('passwordr');
        if (empty($name) || empty($password)) {
            $error="password or username can not be empty";
            $this->index($error);
        } else {
            $this->load->model("registermodel");
            $this->registermodel->register($name, $password);
            redirect(base_url() . "index.php");
        }
    }

    //verification 
    //put your code here
}
