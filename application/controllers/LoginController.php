<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginController extends CI_Controller {

    public function index() {
        $this->load->view('login');
    }

    public function verifyUser() {
        $name = $this->input->post('username');
        $password = $this->input->post('password');

        $this->load->model("loginmodel");
        if ($this->loginmodel->login($name, $password)) {
            if (!isset($_SESSION)) {
                session_start();
            }
            $_SESSION['username'] = $name;
            redirect(base_url() . "index.php");
            return true;
        } else {
            $this->load->view("head_view");
            $this->load->model("generalsql_model");
            $this->loadNavigation();
            $data['error']="error login,wrong password or username";
            $this->load->view("error_view",$data);
            $this->load->view("footer_view");
            return false;
        }
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function Logout() {
        session_start();
        session_destroy();
        redirect(base_url() . "index.php");
    }

}
