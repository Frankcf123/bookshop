<?php


class result_category_controller extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index($id="",$title="") {

        $this->initPage($id,$title);
    }

    public function initPage($id="",$title="") {
        $this->load->view("head_view");
        $this->load->model("generalsql_model");
        $this->loadNavigation();
        $this->loadResult($id,$title);
        $this->load->view("footer_view");
    }

    public function loadNavigation() {
        $this->load->model("generalsql_model");

        $data['categories'] = $this->generalsql_model->getAllCategories();
        $this->load->view("navigation_view", $data);
    }

    public function loadResult($id="",$title=""){
    
        //load reviews
        $this->load->library('pagination');
        $config['base_url'] = base_url()."/index.php/result_category_controller/index/".$id;
        $config['per_page'] = 4;
        $config['num_links'] = 5;
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('category_id',$id);
        $config['total_rows'] = $this->db->get()->num_rows();
        $this->db->select('*');
        $this->db->from('book');
        $this->db->where('category_id',$id);
        $this->db->limit(10,$this->uri->segment(4));
        $data['query']= $this->db->get();
       $this->pagination->initialize($config);
       $data['title']=$title;
       
        $this->load->view("result_view", $data);
    }
    //put your code here
}
